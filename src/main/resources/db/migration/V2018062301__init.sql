CREATE TABLE application_info (
    id VARCHAR(50) PRIMARY KEY,
    name VARCHAR (255) NOT NULL ,
    value VARCHAR (255) NOT NULL,
    created_date BIGINT,
    created_by VARCHAR (255),
    last_modified_date BIGINT,
    last_modified_by VARCHAR (255)
);

CREATE TABLE fund_provider (
    id VARCHAR(50) PRIMARY KEY,
    full_name varchar (5),
    email varchar (50) UNIQUE ,
    phone varchar (15),
    password varchar (255),
    created_date BIGINT,
    created_by VARCHAR (255),
    last_modified_date BIGINT,
    last_modified_by VARCHAR (255)
);
