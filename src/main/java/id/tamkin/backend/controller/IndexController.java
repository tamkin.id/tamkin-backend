package id.tamkin.backend.controller;

import id.tamkin.backend.command.index.IndexCommand;
import id.tamkin.backend.core.command.CommandExecutor;
import id.tamkin.backend.model.command.index.IndexCommandRequest;
import id.tamkin.backend.model.command.index.IndexCommandResponse;
import id.tamkin.backend.model.controller.WebResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.security.Principal;

@RestController
public class IndexController {

    @Autowired
    private CommandExecutor commandExecutor;

    @GetMapping(
        value = "/",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Mono<WebResponse<IndexCommandResponse>> index(Principal principal) {
        return commandExecutor.execute(IndexCommand.class, toIndexRequest(principal))
            .map(WebResponse::ok)
            .subscribeOn(Schedulers.elastic());
    }

    private IndexCommandRequest toIndexRequest(Principal principal) {
        return IndexCommandRequest.builder()
            .name(principal.getName())
            .build();
    }

}
