package id.tamkin.backend.core.security;

import id.tamkin.backend.entity.ApplicationClient;
import id.tamkin.backend.repository.ApplicationClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class ApplicationClientDetailService implements ClientDetailsService {

    @Autowired
    private ApplicationClientRepository repository;

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        return repository.findByName(clientId)
            .map(this::toClientDetails)
            .orElseThrow(() -> new ClientRegistrationException("Client id is not found"));
    }

    private ClientDetails toClientDetails(ApplicationClient applicationClient) {
        BaseClientDetails clientDetails = new BaseClientDetails();
        clientDetails.setClientId(applicationClient.getName());
        clientDetails.setScope(Arrays.asList("read", "write"));
        clientDetails.setAuthorizedGrantTypes(Arrays.asList("password", "refresh_token"));
        return clientDetails;
    }
}
