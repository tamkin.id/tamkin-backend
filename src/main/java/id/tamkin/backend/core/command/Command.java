package id.tamkin.backend.core.command;

import id.tamkin.backend.util.ReactiveUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * @author Eko Kurniawan Khannedy
 */
public interface Command<REQ, RES> {

    Mono<RES> execute(REQ request);

    default <T> Mono<T> mono(Supplier<T> supplier) {
        return ReactiveUtils.mono(supplier);
    }

    default <T> Flux<T> fluxStream(Supplier<Stream<? extends T>> supplier) {
        return ReactiveUtils.fluxStream(supplier);
    }

    default <T> Flux<T> flux(Supplier<Collection<? extends T>> supplier) {
        return ReactiveUtils.flux(supplier);
    }

}
