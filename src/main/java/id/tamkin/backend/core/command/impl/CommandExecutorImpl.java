package id.tamkin.backend.core.command.impl;

import id.tamkin.backend.core.command.Command;
import id.tamkin.backend.core.command.CommandExecutor;
import id.tamkin.backend.error.ValidationException;
import id.tamkin.backend.util.ReactiveUtils;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

@Component
public class CommandExecutorImpl implements CommandExecutor, ApplicationContextAware {

    @Autowired
    private Validator validator;

    @Setter
    private ApplicationContext applicationContext;

    @Override
    public <REQ, RES> Mono<RES> execute(Class<? extends Command<REQ, RES>> commandClass, REQ request) {
        return validateRequestAsMono(request)
            .flatMap(success -> executeCommand(commandClass, request));
    }

    private <REQ, RES> Mono<RES> executeCommand(Class<? extends Command<REQ, RES>> commandClass, REQ request) {
        return applicationContext.getBean(commandClass).execute(request);
    }

    private Mono<Boolean> validateRequestAsMono(Object request) {
        return ReactiveUtils.mono(() -> validateRequest(request));
    }

    private boolean validateRequest(Object request) {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(request);
        if (!constraintViolations.isEmpty()) {
            throw new ValidationException(constraintViolations);
        } else {
            return true;
        }
    }
}
