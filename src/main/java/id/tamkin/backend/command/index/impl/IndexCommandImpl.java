package id.tamkin.backend.command.index.impl;

import id.tamkin.backend.command.index.IndexCommand;
import id.tamkin.backend.model.command.index.IndexCommandRequest;
import id.tamkin.backend.model.command.index.IndexCommandResponse;
import org.springframework.stereotype.Component;

@Component
public class IndexCommandImpl implements IndexCommand {

    @Override
    public IndexCommandResponse doExecute(IndexCommandRequest request) {
        return toResponse(request.getName());
    }

    private IndexCommandResponse toResponse(String name) {
        return IndexCommandResponse.builder()
            .hello(String.format("Hello %s", name))
            .build();
    }
}
