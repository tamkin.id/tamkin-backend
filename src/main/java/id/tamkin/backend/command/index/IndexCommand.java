package id.tamkin.backend.command.index;

import id.tamkin.backend.core.command.BlockingCommand;
import id.tamkin.backend.core.command.Command;
import id.tamkin.backend.model.command.index.IndexCommandRequest;
import id.tamkin.backend.model.command.index.IndexCommandResponse;

public interface IndexCommand extends BlockingCommand<IndexCommandRequest, IndexCommandResponse> {

}
