package id.tamkin.backend.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotBlank;

@ConfigurationProperties("tamkin.security")
@Data
public class SecurityProperties {

    @NotBlank
    private String jwtSigningKey;

}
