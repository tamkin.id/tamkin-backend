package id.tamkin.backend.error;

public class TamkinException extends RuntimeException {

    public TamkinException() {
    }

    public TamkinException(String message) {
        super(message);
    }

    public TamkinException(String message, Throwable cause) {
        super(message, cause);
    }

    public TamkinException(Throwable cause) {
        super(cause);
    }
}
