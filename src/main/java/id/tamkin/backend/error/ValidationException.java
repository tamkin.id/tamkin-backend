package id.tamkin.backend.error;

import id.tamkin.backend.util.ValidationUtils;
import lombok.Getter;

import javax.validation.ConstraintViolation;
import java.util.Set;

public class ValidationException extends TamkinException {

    @Getter
    private Set<ConstraintViolation<?>> constraintViolations;

    @SuppressWarnings("unchecked")
    public ValidationException(Set constraintViolations) {
        this(ValidationUtils.errors(constraintViolations).toString(), constraintViolations);
    }

    @SuppressWarnings("unchecked")
    public ValidationException(String message, Set constraintViolations) {
        super(message);
        this.constraintViolations = constraintViolations;
    }
}
