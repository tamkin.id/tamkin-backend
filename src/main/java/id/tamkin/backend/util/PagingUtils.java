package id.tamkin.backend.util;

import id.tamkin.backend.model.paging.Paging;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public class PagingUtils {

    public static Paging toPaging(Pageable pageable, Page page) {
        return Paging.builder()
            .page(page.getPageable().getPageNumber())
            .size(page.getSize())
            .totalItem((int) page.getTotalElements())
            .totalPage(page.getTotalPages())
            .build();
    }

}
