package id.tamkin.backend.repository;

import id.tamkin.backend.entity.ApplicationInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationInfoRepository extends JpaRepository<ApplicationInfo, String> {

}
