package id.tamkin.backend.repository;

import id.tamkin.backend.entity.FundProvider;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FundProviderRepository extends JpaRepository<FundProvider, String> {

}
