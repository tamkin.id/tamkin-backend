package id.tamkin.backend.repository;

import id.tamkin.backend.entity.ApplicationClient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ApplicationClientRepository extends JpaRepository<ApplicationClient, String> {

    Optional<ApplicationClient> findByName(String name);

}
