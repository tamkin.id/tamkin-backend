package id.tamkin.backend.model.paging;

import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Paging {

    private Integer page;

    private Integer size;

    private Integer totalPage;

    private Integer totalItem;

}
