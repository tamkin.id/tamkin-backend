package id.tamkin.backend.model.command.index;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IndexCommandRequest {

    @NotBlank(message = "NotBlank")
    private String name;
}
