package id.tamkin.backend;

import id.tamkin.backend.properties.SecurityProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
@EnableConfigurationProperties({
    SecurityProperties.class
})
public class TamkinBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(TamkinBackendApplication.class, args);
    }
}
