package id.tamkin.backend.core.command.impl;

import id.tamkin.backend.core.command.Command;
import id.tamkin.backend.core.command.CommandExecutor;
import id.tamkin.backend.error.ValidationException;
import lombok.Builder;
import lombok.Data;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotBlank;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CommandExecutorImplTest.Application.class)
public class CommandExecutorImplTest {

    @Autowired
    private CommandExecutor commandExecutor;

    @Test(expected = ValidationException.class)
    public void testValidationError() {
        HelloRequest request = HelloRequest.builder().build();
        commandExecutor.execute(HelloCommand.class, request).block();
    }

    @Test
    public void testCommand() {
        HelloRequest request = HelloRequest.builder().name("Eko").build();
        HelloResponse response = commandExecutor.execute(HelloCommand.class, request).block();

        assertEquals("Hello Eko", response.getResponse());
    }

    @Data
    @Builder
    public static class HelloRequest {

        @NotBlank
        private String name;

    }

    @Data
    @Builder
    public static class HelloResponse {

        private String response;

    }

    public static interface HelloCommand extends Command<HelloRequest, HelloResponse> {

    }

    @SpringBootApplication(
        exclude = {
            DataSourceAutoConfiguration.class,
            JpaRepositoriesAutoConfiguration.class,
            FlywayAutoConfiguration.class
        }
    )
    public static class Application {

        @Component
        public static class HelloCommandImpl implements HelloCommand {

            @Override
            public Mono<HelloResponse> execute(HelloRequest request) {
                return mono(() -> HelloResponse.builder().response("Hello " + request.getName()).build());
            }
        }

    }

}
